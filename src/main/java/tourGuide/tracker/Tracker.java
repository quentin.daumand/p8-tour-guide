package tourGuide.tracker;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tourGuide.service.TourGuideService;
import tourGuide.model.UserModel;

public class Tracker extends Thread {
	private Logger LOGGER = LoggerFactory.getLogger(Tracker.class);
	private static final long TRACKING_POLLING_INTERVAL = TimeUnit.MINUTES.toSeconds(5);
	private boolean STOP = false;

	private final ExecutorService executorService = Executors.newSingleThreadExecutor();
	private final TourGuideService tourGuideService;

	public Tracker(TourGuideService tourGuideService) {
		this.tourGuideService = tourGuideService;
	}

	/**
	 * Starting the tracker
	 */
	public void startTracking() {
		STOP = false;
		executorService.submit(this);
	}

	/**
	 * Assures to shut down the Tracker thread
	 */
	public void stopTracking() {
		STOP = true;
		executorService.shutdownNow();
	}
	/**
	 * Override of the run method once Tracker class is launched
	 * Get a list of all users
	 * Start a StopWatch to count the time
	 * You can either track the time for tracking user locations or calculate rewards
	 * Reset the StopWatch and put to sleep the thread for the time defined in trackingPollingInterval (in seconds)
	 */
	@Override
	public void run() {
		StopWatch stopWatch = new StopWatch();
		while(true) {
			if(Thread.currentThread().isInterrupted() || STOP) {
				LOGGER.debug("Tracker stopping");
				break;
			}

			List<UserModel> users = tourGuideService.getAllUsers();
			LOGGER.debug("Begin Tracker. Tracking " + users.size() + " users.");
			stopWatch.start();
			try {
				tourGuideService.trackListUserLocation(users);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			stopWatch.stop();
			LOGGER.debug("Tracker Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
			stopWatch.reset();
			try {
				LOGGER.debug("Tracker sleeping");
				TimeUnit.SECONDS.sleep(TRACKING_POLLING_INTERVAL);
			} catch (InterruptedException e) {
				break;
			}
		}
		
	}
}
