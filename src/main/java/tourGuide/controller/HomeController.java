package tourGuide.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    private Logger logger = LoggerFactory.getLogger(HomeController.class);


    /** HTML GET request that returns a welcome message
     *
     * @return a string message
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        logger.debug("Access to / endpoint");
        return "Greetings from TourGuide!";
    }

}
