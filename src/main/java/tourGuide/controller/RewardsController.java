package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.exceptions.UserNotFoundException;
import tourGuide.service.InternalTestService;
import tourGuide.service.TourGuideService;

@RestController
public class RewardsController {
    private Logger LOGGER = LoggerFactory.getLogger(RewardsController.class);

    @Autowired
    TourGuideService tourGuideService;

    @Autowired
    InternalTestService internalTestService;

    /** HTML GET request that returns the rewards of the username bounded to the request
     *
     * @param userName string of the username (internalUserX)
     * @return a Json string of all UserRewards
     */
    @RequestMapping(value="/getRewards", method = RequestMethod.GET)
    public String getRewards(@RequestParam String userName) throws UserNotFoundException {
        LOGGER.debug("Access to /getRewards endpoint with username : " + userName);

        if (!internalTestService.checkIfUserNameExists(userName)) {
            throw new UserNotFoundException(userName);
    }
        return JsonStream.serialize(tourGuideService.getUserRewards(tourGuideService.getUser(userName)));
    }
}
