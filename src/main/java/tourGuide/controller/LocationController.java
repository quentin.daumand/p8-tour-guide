package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.exceptions.UserNotFoundException;
import tourGuide.model.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tourGuide.model.UserModel;
import tourGuide.service.InternalTestService;
import tourGuide.service.TourGuideService;


@RestController
public class LocationController {
    private Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    TourGuideService tourGuideService;

    @Autowired
    InternalTestService internalTestService;

    /**
     * HTML GET request that starts the tracker
     */
    @RequestMapping(value = "/location/startTracker", method = RequestMethod.GET)

    public void startTracker() {
        tourGuideService.tracker.startTracking();
        LOGGER.debug("Starting tracker");
    }

    /**
     * HTML GET request that stops the tracker
     */
    @RequestMapping(value = "/location/stopTracker", method = RequestMethod.GET)
    public void stopTracker() {
        tourGuideService.tracker.stopTracking();
    }


    /** HTML GET request that returns a random location of the username bounded to the request
     *
     * @param userName string of the username (internalUserX)
     * @return a Json string of a user location in longitude and latitude
     */
    @RequestMapping(value= "/getLocation", method = RequestMethod.GET)
    public String getLocation(@RequestParam String userName) throws UserNotFoundException {
        LOGGER.debug("Access to /getLocation endpoint with username : " + userName);

        if (!internalTestService.checkIfUserNameExists(userName)) {
            throw new UserNotFoundException(userName);
        }
        VisitedLocation visitedLocation = tourGuideService.getUserLocationVisited(tourGuideService.getUser(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    /** HTML GET request that returns the 5 closest attractions of the username bounded to the request
     *
     * @param userName string of the username (internalUserX)
     * @return a Json string of nearby attractions.
     */
    @RequestMapping(value= "/getNearbyAttractions", method = RequestMethod.GET)
    public String getNearbyAttractions(@RequestParam String userName) throws UserNotFoundException {
        LOGGER.debug("Access to /getNearbyAttractions endpoint with username : " + userName);

        if (!internalTestService.checkIfUserNameExists(userName)) {
            throw new UserNotFoundException(userName);
        }
        UserModel user = tourGuideService.getUser(userName);
        VisitedLocation visitedLocation = tourGuideService.getUserLocationVisited(user);
        return JsonStream.serialize(tourGuideService.getNearestAttractionsFromUser(visitedLocation, user));
    }


    /** HTML GET request that returns the current location of all users
     *
     * @return a Json string of current location of all users.
     */
    @RequestMapping(value="/getAllCurrentLocations", method = RequestMethod.GET)
    public String getAllCurrentLocations() {
        LOGGER.debug("Access to /getAllCurrentLocations endpoint");

        return JsonStream.serialize(tourGuideService.getAllUsersLocations());
    }

}
