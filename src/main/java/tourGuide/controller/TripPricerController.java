package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.exceptions.UserNotFoundException;
import tourGuide.service.InternalTestService;
import tourGuide.service.TourGuideService;
import tourGuide.model.trip.Provider;

import java.util.List;

@RestController
public class TripPricerController {
    private Logger LOGGER = LoggerFactory.getLogger(TripPricerController.class);

    @Autowired
    TourGuideService tourGuideService;

    @Autowired
    InternalTestService internalTestService;

    /** HTML GET request that returns 5 random trip deals of the username bounded to the request
     *
     * @param userName string of the username (internalUserX)
     * @return a string of a list of Provider in a random way
     */
    @RequestMapping(value="/getTripDeals", method = RequestMethod.GET)
    public String getTripDeals(@RequestParam String userName) throws UserNotFoundException {
        LOGGER.debug("Access to /getTripDeals endpoint with username : " + userName);
        if(!internalTestService.checkIfUserNameExists(userName)) {
            LOGGER.error("This username does not exist" + userName);
            throw new UserNotFoundException(userName);
        }
        List<Provider> providers = tourGuideService.getTripDeals(tourGuideService.getUser(userName));
        return JsonStream.serialize(providers);
    }
}
