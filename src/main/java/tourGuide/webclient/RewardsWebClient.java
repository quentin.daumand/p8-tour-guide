package tourGuide.webclient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.UUID;

@Service
public class RewardsWebClient {
    private final String BASE_URL = "http://localhost:8082";
    //private String BASE_URL = "http://rewards:8082";
    private final String PATH = "/getRewardPoints";

    public int getRewardPointsWebClient(UUID attractionId, UUID userId) {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        int rewardPoints;

        ResponseEntity<Integer> result  =
                restTemplate.getForEntity(BASE_URL + PATH +
                                "?attractionId=" +
                                attractionId +
                                "&userId=" +
                                userId
                        ,Integer.class);

        rewardPoints = result.getBody();
        return rewardPoints;
    }
}
