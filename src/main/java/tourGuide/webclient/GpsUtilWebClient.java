package tourGuide.webclient;

import org.springframework.stereotype.Service;
import tourGuide.model.location.Attraction;
import tourGuide.model.location.VisitedLocation;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class GpsUtilWebClient {

    private String BASE_URL = "http://localhost:8081";
    //private String BASE_URL = "http://gps:8081";
    private String PATH = "/getUserLocation";

    public VisitedLocation getUserLocationVisited(UUID userID) {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        VisitedLocation visitedLocation;

        ResponseEntity<VisitedLocation> result  =
                restTemplate.getForEntity(BASE_URL + PATH +
                                "?userId=" +
                                userID
                        ,VisitedLocation.class);
        visitedLocation = result.getBody();
        return visitedLocation;
    }

    public List<Attraction> getAllAttractions() {
        RestTemplate restTemplate = new RestTemplate();
        List<Attraction> attractionList;

        ResponseEntity<List<Attraction>> result =
                restTemplate.exchange(BASE_URL + "/getAllAttractions",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Attraction>>() {
                        });
        attractionList= result.getBody();
        return attractionList;
    }
}
