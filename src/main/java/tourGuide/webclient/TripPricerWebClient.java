package tourGuide.webclient;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tourGuide.model.trip.Provider;

import java.util.List;
import java.util.UUID;

@Service
public class TripPricerWebClient {

    private String BASE_URL = "http://localhost:8083";
    //private String BASE_URL = "http://pricer:8083";
    private String PATH = "/getPrice";

    public List<Provider> getPriceWebClient(String apiKey, UUID attractionId, int adults, int children, int nightsStay,
                                            int rewardsPoints) {
        RestTemplate restTemplate = new RestTemplate();
        List<Provider> listProvider;

        ResponseEntity<List<Provider>> result =
                restTemplate.exchange(BASE_URL + PATH
                                + "?apiKey=" + apiKey
                                + "&attractionId=" + attractionId
                                + "&adults=" + adults
                                + "&children=" + children
                                + "&nightsStay=" + nightsStay
                                + "&rewardsPoints=" + rewardsPoints,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Provider>>() {
                        });
        listProvider= result.getBody();
        return listProvider;
    }
}
