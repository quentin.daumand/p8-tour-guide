package tourGuide.service;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tourGuide.model.location.Attraction;
import tourGuide.model.UserLocationModel;
import tourGuide.model.location.VisitedLocation;
import tourGuide.model.UserNearestAttractionsModel;
import tourGuide.tracker.Tracker;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;
import tourGuide.webclient.GpsUtilWebClient;
import tourGuide.webclient.TripPricerWebClient;
import tourGuide.model.trip.Provider;

@Service
public class TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private int NB_NEAREST_ATTRACTIONS = 5;
	boolean TEST_MODE = true;

	public Tracker tracker = new Tracker(this);

	private GpsUtilWebClient gpsUtilWebClient;
	private TripPricerWebClient tripPricerWebClient;
	private InternalTestService internalTestService;
	private RewardsService rewardsService;

	ExecutorService executorService = Executors.newFixedThreadPool(100);
	/**
	 * Constructor of the class TourGuideService for initializing users
	 * if testMode (default value = true) then initializeInternalUsers based on internalUserNumber value in
	 * InternalTestHelper
	 * Initialize Tracker
	 * Ensure that the thread Tracker shuts down by calling addShutDownHook before closing the JVM
	 * @param rewardsService
	 */
	public TourGuideService(GpsUtilWebClient gpsUtilWebClient, RewardsService rewardsService, InternalTestService internalTestService, TripPricerWebClient tripPricerWebClient) {
		this.gpsUtilWebClient = gpsUtilWebClient;
		this.rewardsService = rewardsService;
		this.internalTestService = internalTestService;
		this.tripPricerWebClient = tripPricerWebClient;
		
		if(TEST_MODE) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			internalTestService.initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker.startTracking();
		addShutDownHook();
	}
	/**
	 * Get the UserRewards of the concerned user
	 * @param user
	 * @return a list of UserRewards
	 */
	public List<UserRewardModel> getUserRewards(UserModel user) {
		return user.getUserRewards();
	}
	
	public VisitedLocation getUserLocationVisited(UserModel user) {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() :
			trackUserLocation(user);
		return visitedLocation;
	}

	/**
	 * Get a single user from InternalUserMap
	 * @param userName
	 * @return a user
	 */
	public UserModel getUser(String userName) {
		return internalTestService.internalUserMap.get(userName);
	}

	/**
	 * Get a list of all users from the InternalUserMap
	 * @return a list of users
	 */
	public List<UserModel> getAllUsers() {
		return internalTestService.internalUserMap.values().stream().collect(Collectors.toList());
	}

	/**
	 * Get a list of Trip Deals in a form of a list of Providers according to the user preferences
	 * @param user the concerned user
	 * @return list of Provider
	 */
	public List<Provider> getTripDeals(UserModel user) {
		int cumulativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricerWebClient.getPriceWebClient(internalTestService.TRIP_PRICER_API_KEY, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}
	/**
	 * Get the UserLocation from GpsUtil, add it to the visitedLocation and calculate the Rewards
	 * @param user
	 * @return the visited location of the random location of user
	 */
	public VisitedLocation trackUserLocation(UserModel user) {
		VisitedLocation visitedLocation = gpsUtilWebClient.getUserLocationVisited(user.getUserId());
		user.addToVisitedLocations(visitedLocation);
		rewardsService.calculateRewards(user);
		return visitedLocation;
	}
	/**
	 * Create an ExecutorService thread pool in which a runnable of trackUserLocation is executed
	 * @param userList the list containing all users
	 * @throws InterruptedException
	 */
	public void trackListUserLocation(List<UserModel> userList) throws InterruptedException {
		for (UserModel user: userList) {
			Runnable runnable = () -> {
				trackUserLocation(user);
			};
			executorService.execute(runnable);
		}

	}
	/**
	 * Get the closest 5 attractions of the user
	 * @param visitedLocation
	 * @return a list of attractions
	 */
	public List<UserNearestAttractionsModel> getNearestAttractionsFromUser(VisitedLocation visitedLocation, UserModel user) {
		List<Attraction> attractions = gpsUtilWebClient.getAllAttractions();
		List<UserNearestAttractionsModel> nearestAttractions = new ArrayList<>();

		nearestAttractions = attractions.parallelStream().map(
				att -> new UserNearestAttractionsModel(att.attractionName, att.longitude, att.latitude, visitedLocation.location,
						rewardsService.getDistance(att, visitedLocation.location), rewardsService.getRewardPoints(att, user))
		).sorted(Comparator.comparing(UserNearestAttractionsModel::getAttractionDistance))
				.collect(Collectors.toList());
		nearestAttractions = nearestAttractions.stream().limit(NB_NEAREST_ATTRACTIONS).collect(Collectors.toList());

		return nearestAttractions;
	}
	/**
	 * Add a shut down hook for stopping the Tracker thread before shutting down the JVM
	 */
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}

	/**
	 * Get a list of all user Locations from the existent GpsUtil list (tracking locations every X second)
	 * @return a list of UserLocationModel containing all user ID's and location
	 */
	public List<UserLocationModel> getAllUsersLocations() {
		List<UserModel> userModelList = getAllUsers();
		List<UserLocationModel> userLocationModelList = new ArrayList<>();

		userModelList.forEach(user -> {
			userLocationModelList.add(new UserLocationModel(user.getLastVisitedLocation().location, user.getUserId().toString()));
		});

		return userLocationModelList;
	}
}
