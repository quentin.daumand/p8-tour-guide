package tourGuide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import tourGuide.model.location.Attraction;
import tourGuide.model.location.Location;
import tourGuide.model.location.VisitedLocation;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;
import tourGuide.webclient.GpsUtilWebClient;
import tourGuide.webclient.RewardsWebClient;

@Service
public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
    private final int DEFAULT_PROXIMITY_BUFFER = 10;
	private int proximityBuffer = DEFAULT_PROXIMITY_BUFFER;
	private int ATTRACTION_PROXIMITY_RANGE = 200;

	private final GpsUtilWebClient gpsUtilWebClient;
	private final RewardsWebClient rewardsWebClient;
	
	public RewardsService(GpsUtilWebClient gpsUtilWebClient, RewardsWebClient rewardsWebClient) {
		this.gpsUtilWebClient = gpsUtilWebClient;
		this.rewardsWebClient = rewardsWebClient;
	}
	
	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}
	
	public void setDefaultProximityBuffer() {
		proximityBuffer = DEFAULT_PROXIMITY_BUFFER;
	}

	/**
	 * Calculate the rewards for each attraction in the visited location list
	 * @param user the user model
	 */
	public void calculateRewards(UserModel user) {
		List<VisitedLocation> userLocations = user.getVisitedLocations();
		List<Attraction> attractions = gpsUtilWebClient.getAllAttractions();

		userLocations.forEach(visitedLocation -> {
			attractions.forEach(attraction -> {
				if (user.getUserRewards().stream().filter(userRewardModel -> userRewardModel.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
					if (nearAttraction(visitedLocation,attraction)) {
						user.addUserReward(new UserRewardModel(visitedLocation, attraction, getRewardPoints(attraction, user)));
					}
				}
			});
		});
	}
	/**
	 * Compare the distance between an attraction/location and the attraction proximity range
	 * @param attraction
	 * @param location
	 * @return boolean if location is within attraction range
	 */
	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > ATTRACTION_PROXIMITY_RANGE ? false : true;
	}

	/**
	 * Compare the distance between an attraction/visited location and the proximity buffer
	 * @param visitedLocation
	 * @param attraction
	 * @return boolean if visited location is within attraction range
	 */
	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}
	/**
	 * Set a random reward point
	 * @param attraction non-used at the moment
	 * @param user non-used at the moment
	 * @return int of a reward point
	 */
	public int getRewardPoints(Attraction attraction, UserModel user) {
		return rewardsWebClient.getRewardPointsWebClient(attraction.attractionId, user.getUserId());
	}
	/**
	 * Get the distance between two locations
	 * @param loc1 location 1 with latitude and longitude data
	 * @param loc2 location 2 with latitude and longitude data
	 * @return
	 */
	public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                               + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
        return statuteMiles;
	}

}
