package tourGuide.helper;

/**
 * Class used for test purposes
 *  You can set here the internal user number
 */
public class InternalTestHelper {

	// Set this default up to 100,000 for testing
	private static int INTERNAL_USER_NUMBER = 100000;
	
	public static void setInternalUserNumber(int internalUserNumber) {
		InternalTestHelper.INTERNAL_USER_NUMBER = internalUserNumber;
	}
	
	public static int getInternalUserNumber() {
		return INTERNAL_USER_NUMBER;
	}
}
