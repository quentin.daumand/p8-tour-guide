package tourGuide.model;

public class UserPreferencesModel {
	
	private int tripDuration = 1;
	private int numberOfAdults = 1;
	private int numberOfChildren = 0;
	
	public UserPreferencesModel() {
	}
	
	public int getTripDuration() {
		return tripDuration;
	}

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public int getNumberOfChildren() {
		return numberOfChildren;
	}

}
