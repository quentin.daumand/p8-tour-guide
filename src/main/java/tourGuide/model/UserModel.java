package tourGuide.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import tourGuide.model.location.VisitedLocation;
import tourGuide.model.trip.Provider;

public class UserModel {
	private final UUID userId;
	private final String userName;
	private String phoneNumber;
	private String emailAddress;
	private List<VisitedLocation> visitedLocations = new ArrayList<>();
	private List<UserRewardModel> userRewards = new ArrayList<>();
	private UserPreferencesModel userPreferences = new UserPreferencesModel();
	private List<Provider> tripDeals = new ArrayList<>();

	public UserModel(UUID userId, String userName, String phoneNumber, String emailAddress) {
		this.userId = userId;
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
	}
	
	public UUID getUserId() {
		return userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void addToVisitedLocations(VisitedLocation visitedLocation) {
		visitedLocations.add(visitedLocation);
	}
	
	public List<VisitedLocation> getVisitedLocations() {
		return visitedLocations;
	}
	
	public void addUserReward(UserRewardModel userReward) {
			userRewards.add(userReward);
	}
	
	public List<UserRewardModel> getUserRewards() {
		return userRewards;
	}
	
	public UserPreferencesModel getUserPreferences() {
		return userPreferences;
	}
	
	public VisitedLocation getLastVisitedLocation() {
		return visitedLocations.get(visitedLocations.size() - 1);
	}
	
	public void setTripDeals(List<Provider> tripDeals) {
		this.tripDeals = tripDeals;
	}
	
}
