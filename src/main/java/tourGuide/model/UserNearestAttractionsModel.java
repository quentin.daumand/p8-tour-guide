package tourGuide.model;

import tourGuide.model.location.Location;

public class UserNearestAttractionsModel {
    private String attractionName;
    private double attractionLongitude;
    private double attractionLatitude;
    private Location location;
    private double attractionDistance;
    private int rewardsPoint;

    public UserNearestAttractionsModel(String attractionName, double attractionLongitude, double attractionLatitude, Location location, double attractionDistance, int rewardsPoint) {
        this.attractionName = attractionName;
        this.attractionLongitude = attractionLongitude;
        this.attractionLatitude = attractionLatitude;
        this.location = location;
        this.attractionDistance = attractionDistance;
        this.rewardsPoint = rewardsPoint;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double getAttractionDistance() {
        return attractionDistance;
    }

}
