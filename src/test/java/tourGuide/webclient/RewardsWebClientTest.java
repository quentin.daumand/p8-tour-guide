package tourGuide.webclient;

import org.apache.commons.lang3.math.NumberUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.service.TourGuideService;

import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RewardsWebClientTest {

    @Autowired
    private RewardsWebClient rewardsWebClient;

    @Autowired
    private TourGuideService tourGuideService;

    @Test
    public void getRewardPointsWebClientShouldReturnFieldsWithValues() throws Exception {

        UUID attractionId = new UUID(4872158, 1875147);
        UUID userId = new UUID(41872158, 18175147);
        int rewardPoints = rewardsWebClient.getRewardPointsWebClient(attractionId, userId);
        String rewardPointsString = String.valueOf(rewardPoints);

        Assertions.assertThat(rewardPoints)
                .isNotNull();
        Assertions.assertThat(NumberUtils.isNumber(rewardPointsString)).isTrue();
    }
}