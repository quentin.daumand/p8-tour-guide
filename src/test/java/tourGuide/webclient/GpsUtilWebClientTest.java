package tourGuide.webclient;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.model.location.Attraction;
import tourGuide.service.TourGuideService;

import java.util.List;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GpsUtilWebClientTest {

    @Autowired
    private GpsUtilWebClient gpsUtilWebClient;

    @Autowired
    private TourGuideService tourGuideService;

    @Test
    public void getUserLocationWebClientShouldReturnFieldsWithValues() throws Exception {
        UUID userId = new UUID(4872158, 1875147);
        Assertions.assertThat(gpsUtilWebClient.getUserLocationVisited(userId))
                .isNotNull()
                .hasFieldOrPropertyWithValue("userId", userId)
                .hasFieldOrProperty("location")
                .hasFieldOrProperty("timeVisited");
    }

    @Test
    public void getAllAttractionsWebClientShouldReturnFieldsWithValues() {
        List<Attraction> attractionList = gpsUtilWebClient.getAllAttractions();
        Assertions.assertThat(attractionList)
                .isNotNull()
                .isNotEmpty();
    }
}
