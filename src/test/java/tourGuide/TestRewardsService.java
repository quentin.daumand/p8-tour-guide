package tourGuide;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import tourGuide.model.location.Attraction;
import tourGuide.model.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.InternalTestService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;
import tourGuide.webclient.GpsUtilWebClient;
import tourGuide.webclient.RewardsWebClient;
import tourGuide.webclient.TripPricerWebClient;

public class TestRewardsService {

	@Test
	public void userGetRewards() {
		GpsUtilWebClient gpsUtilWebClient = new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient = new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());

		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Attraction attraction = gpsUtilWebClient.getAllAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
		tourGuideService.trackUserLocation(user);
		List<UserRewardModel> userRewards = user.getUserRewards();
		tourGuideService.tracker.stopTracking();
		assertTrue(userRewards.size() == 1);
	}
	
	@Test
	public void isWithinAttractionProximity() {
		GpsUtilWebClient gpsUtilWebClient = new GpsUtilWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());
		Attraction attraction = gpsUtilWebClient.getAllAttractions().get(0);
		assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
	}
	
	@Test
	public void nearAllAttractions() {
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient = new TripPricerWebClient();
		GpsUtilWebClient gpsUtilWebClient = new GpsUtilWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);

		InternalTestHelper.setInternalUserNumber(1);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);

		tourGuideService.tracker.stopTracking();

		rewardsService.calculateRewards(tourGuideService.getAllUsers().get(0));
		List<UserRewardModel> userRewards = tourGuideService.getUserRewards(tourGuideService.getAllUsers().get(0));

		assertEquals(gpsUtilWebClient.getAllAttractions().size(), userRewards.size());
	}
	
}
