package tourGuide;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.UUID;

import org.junit.Test;

import tourGuide.model.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserLocationModel;
import tourGuide.model.UserNearestAttractionsModel;
import tourGuide.service.InternalTestService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.UserModel;
import tourGuide.webclient.GpsUtilWebClient;
import tourGuide.webclient.RewardsWebClient;
import tourGuide.webclient.TripPricerWebClient;
import tourGuide.model.trip.Provider;

public class TestTourGuideService {

	@Test
	public void getUserLocation() {
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		tourGuideService.tracker.stopTracking();
		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}
	
	@Test
	public void addUser() {
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		UserModel user2 = new UserModel(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		internalTestService.addUser(user);
		internalTestService.addUser(user2);
		
		UserModel retrivedUser = tourGuideService.getUser(user.getUserName());
		UserModel retrivedUser2 = tourGuideService.getUser(user2.getUserName());

		tourGuideService.tracker.stopTracking();
		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}
	
	@Test
	public void getAllUsers() {
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		RewardsService rewardsService = new RewardsService(gpsUtilWebClient, new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		UserModel user2 = new UserModel(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		internalTestService.addUser(user);
		internalTestService.addUser(user2);
		
		List<UserModel> allUsers = tourGuideService.getAllUsers();

		tourGuideService.tracker.stopTracking();
		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}
	
	@Test
	public void trackUser() {
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(gpsUtilWebClient, new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}
	
	@Test
	public void getNearbyAttractions() {
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(gpsUtilWebClient, new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(1);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		
		List<UserNearestAttractionsModel> attractions = tourGuideService.getNearestAttractionsFromUser(visitedLocation, user);
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(5, attractions.size());
	}

	@Test
	public void getAllUserLocations() {
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(gpsUtilWebClient, new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(10);

		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);

		List<UserLocationModel> userLocationModelList = tourGuideService.getAllUsersLocations();
		tourGuideService.tracker.stopTracking();

		assertEquals(10, userLocationModelList.size());
	}

	@Test
	public void getTripDeals() {
		GpsUtilWebClient gpsUtilWebClient =  new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient =  new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(), new RewardsWebClient());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);
		
		UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<Provider> providers = tourGuideService.getTripDeals(user);
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(5, providers.size());
	}
	
	
}
