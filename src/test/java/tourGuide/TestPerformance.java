package tourGuide;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import tourGuide.model.location.Attraction;
import tourGuide.model.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.InternalTestService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.UserModel;
import tourGuide.webclient.GpsUtilWebClient;
import tourGuide.webclient.RewardsWebClient;
import tourGuide.webclient.TripPricerWebClient;

public class TestPerformance {
	public int numberOfUser = 100000;
	public int nbThread = 50;
	/*
	 * A note on performance improvements:
	 *     
	 *     The number of users generated for the high volume tests can be easily adjusted via this method:
	 *     
	 *     		InternalTestHelper.setInternalUserNumber(100000);
	 *     
	 *     
	 *     These tests can be modified to suit new solutions, just as long as the performance metrics
	 *     at the end of the tests remains consistent. 
	 * 
	 *     These are performance metrics that we are trying to hit:
	 *     
	 *     highVolumeTrackLocation: 100,000 users within 15 minutes:
	 *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
	 *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */
	

	@Test
	public void highVolumeTrackLocation() {
		InternalTestHelper internalTestHelper = new InternalTestHelper();
		internalTestHelper.setInternalUserNumber(numberOfUser);
		RewardsService rewardsService = new RewardsService(new GpsUtilWebClient(),  new RewardsWebClient());
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient = new TripPricerWebClient();;
		GpsUtilWebClient gpsUtilWebClient = new GpsUtilWebClient();
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);

		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		tourGuideService.tracker.stopTracking();

		//Create a list of UserModel containing all users
		List<UserModel> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();

		//Create a stopWatch and start it
	    StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		//Create an executor service with a thread pool of certain amount of threads

		try {
			ExecutorService executorService = Executors.newFixedThreadPool(nbThread);

			//Execute the code as per in the method "trackListUserLocations" in TourGuideService
			//but without the calculation of rewards

			for(UserModel user: allUsers) {
				Runnable runnable = () -> {
					VisitedLocation visitedLocation = gpsUtilWebClient.getUserLocationVisited(user.getUserId());
					user.addToVisitedLocations(visitedLocation);
				};
				executorService.execute(runnable);
			}
			executorService.shutdown();
			executorService.awaitTermination(15, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
		}

		stopWatch.stop();

		//Asserting part that the time is as performant as wanted
		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}


	@Test
	public void highVolumeGetRewards() {
		InternalTestHelper internalTestHelper = new InternalTestHelper();
		internalTestHelper.setInternalUserNumber(numberOfUser);
		GpsUtilWebClient gpsUtilWebClient = new GpsUtilWebClient();
		InternalTestService internalTestService = new InternalTestService();
		TripPricerWebClient tripPricerWebClient = new TripPricerWebClient();
		RewardsService rewardsService = new RewardsService(gpsUtilWebClient, new RewardsWebClient());

		//Create a stopWatch and start it
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		TourGuideService tourGuideService = new TourGuideService(gpsUtilWebClient, rewardsService, internalTestService, tripPricerWebClient);

		tourGuideService.tracker.stopTracking();
		
	    Attraction attraction = gpsUtilWebClient.getAllAttractions().get(0);
		//Create a list of UserModel containing all users
		List<UserModel> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();

		//Create an executor service with a thread pool of certain amount of threads
		try {
			ExecutorService executorService = Executors.newFixedThreadPool(nbThread);

			//Execute the code as per in the method "trackUserLocation" in TourGuideService
			for (UserModel user: allUsers) {
				Runnable runnable = () -> {
					user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
					rewardsService.calculateRewards(user);
					assertTrue(user.getUserRewards().size() > 0);
				};
				executorService.execute(runnable);
			};
			executorService.shutdown();
			executorService.awaitTermination(15, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
		}

		stopWatch.stop();

		//Asserting part that the time is as performant as wanted
		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
	
}
